from flask import Flask
#from flask import request
#from flask import jsonify
import math
import json
import numpy as np
#import random
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import pyplot as plt


app = Flask(__name__)

maxLat = -30
minLat = -36
maxLon = 152
minLon = 146

latNum = 100
lonNum = 120
#resolution = 100
time = 20

x, y = np.meshgrid(np.arange(minLat, maxLat, (maxLat - minLat) / latNum), 
                   np.arange(minLon, maxLon, (maxLon - minLon) / lonNum));

#calculate uncertainty using latitude, longitude and time
def calUncertainty(lat,log,t):
    t1 = lat + 30
    t2 = log - 152
    return np.log10(1+np.exp(-0.002 *t *t1 *t1 - np.power((0.4 * t2 + 0.2*t),2)) 
                    * 3*np.power((t1 + 0.05*t),2))

#calculate possibility using latitude, longitude, time and value
def calPossibility(lat,log, value, t):
    for v in range(1,value):
        temp = None
        if temp is None:
            temp = build_gaussian(lat,log,t)
        else:
            temp = temp + build_gaussian(lat,log,t) 
    
    return temp;

def build_gaussian(lat, lon, t):
    standard_deviation = 0.3 - 0.2 * math.sin((lat+lon+t-110)/11)
    z = np.exp(-((y-lon+ 0.5 * math.cos(t/10))**2 + (x - lat - 0.5*math.sin(t))**2)/(2*(standard_deviation**2)))
    z = 0.7 * z/(np.sqrt(2*np.pi)*standard_deviation);    
    return np.matrix(z);
 
def checkFloat(element):
    try:
        float(element)
        return True
    except ValueError:
        return False
   
def generateJson(lat,lon,poss,unc):
    json_array = [];
    for i in range(0,latNum):
        for j in range(0,lonNum):
            json_array.append({"lat":lat.item((j, i)),
                               "lon":lon.item((j, i)),
                               "pos":poss.item((j, i)),
                               "unc":unc.item((j, i))})
            
    return json_array
    
def getPointsData():
    
    #read json data file
    print("reading file...")
    with open('./data/students_enrollment.json') as json_data:
        d = json.load(json_data)

    #calculate possibility and uncertainty for each point
    print("calculating possibility and uncertainty...")
    
    print(x[0])
    print(x[0].size)
    
    json_data = []
    for t in range(0,time):
        
        print("time:"+str(t))
        poss = None
        for p in d:
            if poss is None and checkFloat(p["Student_number"]):
                poss = calPossibility(float(p["Latitude"]),
                                      float(p["Longitude"]),
                                      int(float(p["Student_number"])),t)
            elif checkFloat(p["Student_number"]):
                poss = poss + calPossibility(float(p["Latitude"]),
                                             float(p["Longitude"]),
                                             int(float(p["Student_number"])),t)
        unc = np.asmatrix(calUncertainty(x,y,t))
        
        poss = 2 * np.log10(np.ones((lonNum,latNum)) + poss)

        fig1 = plt.figure()
        ax1 = Axes3D(fig1)
        ax1.plot_surface(x, y, unc, rstride=1, cstride=1, cmap='rainbow')
        plt.show()
        fig2 = plt.figure()
        ax2 = Axes3D(fig2)
        ax2.plot_surface(x, y, poss, rstride=1, cstride=1, cmap='rainbow')
        plt.show()

        json_object = generateJson(x,y,poss,unc)
        json_data.append({"time":t,"data":json_object})

    json_final = {"settings":{"timeStart":0,"timeEnd":19,"timeStep":1,"latCount":latNum,"lonCount":lonNum},
        "dataSet":json_data}
    
    #write json data file
    print("starting writing file...")
    with open('./data/data.json', 'w') as outfile:  
        json.dump(json_final, outfile)          
    print("finish writing json file")
    
if __name__=='__main__':
    getPointsData()
