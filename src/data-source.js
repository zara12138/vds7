import {json} from 'd3-request';

import data from './data/data';

// const EQUATION0 = (lat, lon, t) => 2000 - 4 * Math.pow(10 * (lat), 2) - 3 * Math.pow(5 * (lon), 2) + 10;
const EQUATION1 = (lat, lon, t) => (Math.pow(3 * (1 - lat), 2) * Math.exp( - (lat * lat) - Math.pow(lon + 1, 2))) * 100;
// const EQUATION = (lat, lon, t) => -10 * lat + 200;
const EQUATION0 = (lat, lon, t) => (Math.pow(3 * (1 - lat), 2) * Math.exp( - (lat * lat) - Math.pow(lon + 1, 2))
    - 10 * (lat / 5 - Math.pow(lat, 3) - Math.pow(lon, 5))
    * Math.exp( - lat * lat - lon * lon)
    - 1 / 3 * Math.exp( - Math.pow(lat + 1, 2) - lon * lon)) * 100;


export function dataSource() {
    return {
        equation0: (lat, lon, time) => EQUATION0(parseFloat(lat) + 33 + parseFloat(time), parseFloat(lon) - 149 + parseFloat(time), parseFloat(time)),
        equation1: (lat, lon, time) => EQUATION1((parseFloat(lat) + 28) * 0.2 - parseFloat(time), (parseFloat(lon) - 160 - parseFloat(time)) * 0.1, parseFloat(time)),
        bound: {
            maxLat: -30,
            minLat: -36,
            maxLon: 152,
            minLon: 146
        },
        data: data
    };
}