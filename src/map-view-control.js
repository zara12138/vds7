import React, {Component} from 'react';

export default class MapViewControl extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pitchBearingIntervalId: 0,
            pitchIntervalId: 0,
            bearingIntervalId: 0,
            oldPitch: 0,
            oldBearing: 0,
            lock: false
        }
    }

    _on2DClick() {
        if (!this.state.lock) {
            this.state.lock = true;
            this.props.onSettingsChange({
                mode2D: true,
                animating: true
            });

            this.state.oldPitch = this.props.viewport.pitch;
            this.state.oldBearing = this.props.viewport.bearing;
            const pitchStep = (0 - this.props.viewport.pitch) / 10;
            const bearingStep = (0 - this.props.viewport.bearing) / 10;
            this.state.pitchBearingIntervalId = setInterval(this._pitchBearingInterval.bind(this, pitchStep, 0, bearingStep, 0), 5);
        }
    }

    _on3DClick() {
        if (!this.state.lock) {
            this.state.lock = true;
            this.props.onSettingsChange({
                mode2D: false,
                animating: true
            });

            const pitchStep = (this.state.oldPitch - this.props.viewport.pitch) / 10;
            const bearingStep = (this.state.oldBearing - this.props.viewport.bearing) / 10;
            this.state.pitchBearingIntervalId = setInterval(this._pitchBearingInterval.bind(this, pitchStep, this.state.oldPitch, bearingStep, this.state.oldBearing), 5);
        }
    }

    _onNaviClick() {
        if (!this.state.lock) {
            this.state.lock = true;
            this.props.onSettingsChange({animating: true});
            const bearingStep = (0 - this.props.viewport.bearing) / 10;
            this.state.pitchBearingIntervalId = setInterval(this._pitchBearingInterval.bind(this, 0, this.props.viewport.pitch, bearingStep, 0), 5);
        }
    }

    _pitchBearingInterval(pitchStep, pitchTarget, bearingStep, bearingTarget) {
        const pitchDiff = pitchTarget - this.props.viewport.pitch;
        const bearingDiff = bearingTarget - this.props.viewport.bearing;
        console.log(pitchDiff, pitchStep, bearingDiff, bearingStep);

        if (Math.abs(pitchDiff) > 2 * Math.abs(pitchStep) || Math.abs(bearingDiff) > 2 * Math.abs(bearingStep)) {
// console.log(pitchDiff, pitchStep, bearingDiff, bearingStep);
            this.props.onViewportChange({
                pitch: this.props.viewport.pitch + pitchStep,
                bearing: this.props.viewport.bearing + bearingStep
            });
        } else {
            this.props.onViewportChange({
                pitch: pitchTarget,
                bearing: bearingTarget
            });
            clearInterval(this.state.pitchBearingIntervalId);
            this.state.lock = false;
console.log("hhhhhhhhhhh");
            this.props.onSettingsChange({animating: false});
        }
    }

    render() {
        const direction = -this.props.viewport.bearing - 45;

        if (this.props.settings.mode2D) {
            return (
                <div>
                    <img src="navigation.png" onClick={() => this._onNaviClick()}
                         style={{transform: "rotate(" + direction + "deg)"}}
                         className="navi-image"/>
                    <button className="btn btn-danger btn-block" style={{marginTop: 10}}
                            onClick={() => this._on3DClick()}>2D
                    </button>
                </div>
            )
        } else {
            return (
                <div>
                    <img src="navigation.png" onClick={() => this._onNaviClick()}
                         style={{transform: "rotate(" + direction + "deg)"}}
                         className="navi-image"/>
                    <button className="btn btn-info btn-block" style={{marginTop: 10}}
                            onClick={() => this._on2DClick()}>3D
                    </button>
                </div>
            )
        }
    }
}