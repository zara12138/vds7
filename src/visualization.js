import React, {Component} from 'react';
import DeckGL, {HexagonLayer} from 'deck.gl';
import PropTypes from 'prop-types';
// import {isWebGL2, registerShaderModules} from 'luma.gl';

import {dataSource} from './data-source';

import DensitySurfaceLayer from './layers/density-surface-layer/density-surface-layer';
import MeshGridSurfaceLayer from './layers/mesh-grid-surface-layer/mesh-grid-surface-layer';

const propTypes = {
    viewport: PropTypes.object.isRequired,
    settings: PropTypes.object.isRequired
};

// const LIGHT_SETTINGS = {
//     lightsPosition: [-0.144528, 49.739968, 8000, -3.807751, 54.104682, 8000],
//     ambientRatio: 0.4,
//     diffuseRatio: 0.6,
//     specularRatio: 0.2,
//     lightsStrength: [0.8, 0.0, 0.8, 0.0],
//     numberOfLights: 2
// };

const colorRange = [
    [1, 152, 189],
    [73, 227, 206],
    [216, 254, 181],
    [254, 237, 177],
    [254, 173, 84],
    [209, 55, 78]
];

export default class Visualization extends Component {

    constructor(props) {
        super(props);

        this.state = {
            equation0: dataSource().equation0,
            equation1: dataSource().equation1,
            data: dataSource().data,
            hoverInfo: null
        };
    }

    componentDidMount() {
        // this.setState(this.state.getData(this.props));
    }

    static getDerivedStateFromProps(nextProps, prevState) {
        // if (nextProps.settings.time !== prevState.time) {
        //     return prevState.getData(nextProps);
        // }
        return null;
    }

    componentWillUnmount() {}

    _onHover(info) {
        const hoverInfo = info.picked ? info : null;
        if (hoverInfo !== null) {
            // hoverInfo.value = dataSource().equation0(info.lngLat[1], info.lngLat[0], this.state.time);
        }
// console.log(hoverInfo);
        if (hoverInfo !== this.state.hoverInfo) {
            this.setState({hoverInfo});
        }
    }

    render() {
        const {viewport, settings} = this.props;

        const {data, equation0, equation1, hoverInfo} = this.state;

        const layers = [
            settings.showDensitySurface && new DensitySurfaceLayer({
                id: 'density-surface-layer',
                data: {time0: settings.time0},
                getPosition: null,
                getColor: null,
                pickable: true,
                onHover: this._onHover.bind(this),
                bound: {
                    maxLat: -30,
                    minLat: -36,
                    maxLon: 152,
                    minLon: 146
                },
                minResolution: 100,
                getDensity: (lat, lon) => equation0(lat, lon, settings.time0),
                getUncertainty: (lat, lon) => equation1(lat, lon, settings.time0)
            }),
            settings.showMeshGridSurface && new MeshGridSurfaceLayer({
                id: 'mesh-grid-surface-layer',
                data: data.dataSet[settings.time1].data,
                getPosition: (data) => [data.lat, data.lon],
                pickable: true,
                onHover: this._onHover.bind(this),
                latCount: data.settings.latCount,
                lonCount: data.settings.lonCount,
                getDensity: (data) => data.pos * 250, // multiply by 250 so as to make it much more prominent to identify elevation changes
                getUncertainty: (data) => data.unc
            })
        ];

        return (
            <div>
                <DeckGL
                    glOptions={{webgl2: false}}
                    {...viewport}
                    layers={layers}
                    useDevicePixels={true}
                    // onWebGLInitialized={}
                    style={{cursor: "crosshair"}}
                />

                {hoverInfo && (
                    <div className="on-hover-tip" style={{left: hoverInfo.x, top: hoverInfo.y}}>
                        Latitude: {hoverInfo.latLonEle[0].toFixed(6)} <br/>
                        Longitude: {hoverInfo.latLonEle[1].toFixed(6)} <br/>
                        Elevation: {hoverInfo.latLonEle[2].toFixed(6)}
                    </div>
                )}
            </div>
        );
    }
}

Visualization.propTypes = propTypes;