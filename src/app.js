import React, {Component} from 'react';
import autobind from 'react-autobind';
import {render} from 'react-dom';
import MapGL from 'react-map-gl';
import Visualization from "./visualization";
import ControlPanel from './control-panel';
import MapViewControl from './map-view-control';

// Set your mapbox token here
const MAPBOX_TOKEN = process.env.MapboxAccessToken; // eslint-disable-line

class Root extends Component {
    constructor(props) {
        super(props);
        this.state = {
            viewport: {
                width: 500,
                height: 500,
                longitude: 148.225357,
                latitude: -32.556932,
                zoom: 6,
                maxZoom: 16,
                // pitch: 37.11535300402728,
                pitch: 35,
                // bearing: -0.6424747174301046,
                bearing: 0
            },
            settings: {
                mode2D: false,
                animating: false,
                showDensitySurface: true,
                time0: 0,
                showMeshGridSurface: false,
                time1: 0
            }
        };
        autobind(this);
    }

    componentDidMount() {
        window.addEventListener('resize', this._onResize);
        this._onResize();
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this._onResize);
    }

    _onResize() {
        this._updateViewport({
            width: window.innerWidth,
            height: window.innerHeight
        });
    }

    _updateSettings(settings) {
        this.setState({
            settings: {...this.state.settings, ...settings}
        });
    }

    _updateViewport(viewport) {
        this.setState({
            viewport: {...this.state.viewport, ...viewport}
        });
    }

    render() {
        const {viewport, settings} = this.state;
console.log(this.state.settings.mode2D, this.state.settings.animating);
        if (this.state.settings.mode2D && !this.state.settings.animating) {
            viewport.pitch = 0;
        }

        return (
            <div>
                <MapGL
                    {...viewport}
                    mapStyle="mapbox://styles/mapbox/dark-v9"
                    // mapStyle="mapbox://styles/mapbox/light-v9"
                    mapboxApiAccessToken={MAPBOX_TOKEN}
                    dragRotate
                    onViewportChange={this._updateViewport}
                >
                    <Visualization
                        viewport={viewport}
                        settings={settings}
                    />
                </MapGL>

                <div className="control-panel">
                    <ControlPanel settings={settings} onChange={this._updateSettings}/>
                </div>

                <div className="map-view-control">
                    <MapViewControl viewport={viewport} settings={settings} onSettingsChange={this._updateSettings}
                                    onViewportChange={this._updateViewport}/>
                </div>

            </div>
        );
    }
}

render(<Root/>, document.body.appendChild(document.createElement('div')));
