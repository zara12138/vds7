import React, {Component} from 'react';
import PropTypes from 'prop-types';

const propTypes = {
    settings: PropTypes.object.isRequired,
    onChange: PropTypes.func.isRequired,
};

export default class ControlPanel extends Component {
    constructor(props) {
        super(props);
    }

    _renderToggle(key, displayName) {
        return (
            <div className="input">
                <label>{displayName}</label>
                <input
                    type="checkbox"
                    checked={this.props.settings[key] || false}
                    onChange={e => this.props.onChange({[key]: e.target.checked})}
                />
            </div>
        );
    }

    _renderSlider(key, displayName, props) {
        return (
            <div className="input">
                <label>{displayName} ({this.props.settings[key]})</label>
                <input
                    className="range"
                    type="range"
                    {...props}
                    value={this.props.settings[key] || 0}
                    onChange={e => this.props.onChange({[key]: e.target.value})}
                />
            </div>
        );
    }

    render() {
        return (
            <div>
                <a className="btn btn-danger btn-block" href="index.html"><span
                    className="glyphicon glyphicon-chevron-left" aria-hidden="true"></span> Go Back</a>
                <br/>
                {this._renderToggle('showDensitySurface', 'Density Surface')}
                {this._renderSlider('time0', 'Time Line', {min: 0, max: 1, step: 0.001})}
                <hr/>
                {this._renderToggle('showMeshGridSurface', 'Mesh Grid Surface')}
                {this._renderSlider('time1', 'Time Line', {min: 0, max: 19, step: 1})}
            </div>
        );
    }
}

ControlPanel.propTypes = propTypes;
