import {Layer, lighting} from 'deck.gl';
import {GL, Model, Geometry} from 'luma.gl';

import meshGridSurfaceLayerVertex from './mesh-grid-surface-layer-vertex.glsl';
import meshGridSurfaceLayerFragment from './mesh-grid-surface-layer-fragment.glsl';

const DEFAULT_COLOR = [0, 0, 0, 255];
const defaultProps = {
    data: [],
    getPosition: () => [0, 0, 0],
    getColor: () => DEFAULT_COLOR,
    latCount: 0,
    lonCount: 0,
    getDensity: () => 0,
    getUncertainty: () => 0
}

export default class MeshGridSurfaceLayer extends Layer {
    initializeState() {
        const {gl} = this.context;
        const {latCount, lonCount} = this.props;
        const attributeManager = this.getAttributeManager();
        const noAlloc = true;

        attributeManager.add({
            indices: {
                size: 1,
                isIndexed: true,
                update: this.calculateIndices,
                noAlloc
            },
            positions: {
                size: 4,
                accessor: 'getPosition',
                update: this.calculatePositions,
                noAlloc
            },
            colors: {
                size: 4,
                accessor: ['data', 'getColor'],
                type: GL.UNSIGNED_BYTE,
                update: this.calculateColors,
                noAlloc
            },
            pickingColors: {
                size: 3,
                type: GL.UNSIGNED_BYTE,
                update: this.calculatePickingColors,
                noAlloc
            }
        });

        this.setState({
            model: this._getModel(gl),
            latCount: latCount,
            lonCount: lonCount,
            vertexCount: (latCount - 1) * (lonCount - 1) * 2 * 3,
            nodeCount: latCount * lonCount,
            limit: {
                maxElevation: -Infinity,
                minElevation: Infinity,
                maxUncertainty: -Infinity,
                minUncertainty: Infinity
            },
            bound: {
                maxLat: -Infinity,
                minLat: Infinity,
                maxLon: -Infinity,
                minLon: Infinity
            }
        });
    }

    updateState({props, oldProps, changeFlags: {dataChanged, somethingChanged}}) {
        if (props.data !== oldProps.data) {
            this.state.model.setVertexCount(this.state.vertexCount);
// console.log("updateState", this.state);
            this.state.attributeManager.invalidateAll();
            this.setState({
                limit: {
                    maxElevation: -Infinity,
                    minElevation: Infinity,
                    maxUncertainty: -Infinity,
                    minUncertainty: Infinity
                }
            });
        }
    }

    /**
     * For vertex position, see this.calculatePositions(attribute)
     *
     * vertex2 vertex3
     *   |       |
     *   +------.+---
     *   |    _/ |
     *   |  _/   |
     *   | /     |
     *   +'------+---
     *   |       |
     * vertex0 vertex1
     *
     * Scan order: minLon -> maxLon -> nextLat
     * Triangle0: vertex0 -> vertex1 -> vertex3;
     * Triangle1: vertex0 -> vertex2 -> vertex3;
     */
    calculateIndices(attribute) {
        const {} = this.props;
        const {vertexCount, latCount, lonCount} = this.state;
        const indices = new Uint32Array(vertexCount);

        let i = 0;
        for (let latIndex = 0; latIndex < latCount - 1; latIndex++) {
            for (let lonIndex = 0; lonIndex < lonCount - 1; lonIndex++) {
                const vertex0 = latIndex * lonCount + lonIndex;
                const vertex1 = vertex0 + 1;
                const vertex2 = vertex0 + lonCount;
                const vertex3 = vertex2 + 1;

                indices[i++] = vertex0;
                indices[i++] = vertex1;
                indices[i++] = vertex3;

                indices[i++] = vertex0;
                indices[i++] = vertex2;
                indices[i++] = vertex3;
            }
        }
// console.log("index", indices);
        attribute.value = indices;
    }

    /**
     * maxLat
     *   |
     *   ^
     *   |
     *   |
     *   |
     *   +------->
     *   |       |
     * minLat, maxLon
     * minLon
     *
     * Scan order: minLon -> maxLon -> nextLat
     */
    calculatePositions(attribute) {
        const {data, getPosition, getDensity, getUncertainty} = this.props;
        const {nodeCount, limit, bound} = this.state;
        const positions = new Float32Array(nodeCount * attribute.size);

        let i = 0;
        for (let index = 0; index < nodeCount; index++) {
            // Positions order for WebGL should be longitude first
            positions[i++] = getPosition(data[index])[1];
            positions[i++] = getPosition(data[index])[0];

            bound.maxLat = Math.max(bound.maxLat, getPosition(data[index])[0]);
            bound.minLat = Math.min(bound.minLat, getPosition(data[index])[0]);
            bound.maxLon = Math.max(bound.maxLon, getPosition(data[index])[1]);
            bound.minLon = Math.min(bound.minLon, getPosition(data[index])[1]);

            const elevation = getDensity(data[index]);
            const isElevationFinite = isFinite(elevation);
            positions[i++] = isElevationFinite ? elevation : 0;

            const uncertainty = getUncertainty(data[index]);
            const isUncertaintyFinite = isFinite(uncertainty);
            positions[i++] = isUncertaintyFinite ? uncertainty : 0;

            if (isElevationFinite) {
                limit.maxElevation = Math.max(limit.maxElevation, elevation);
                limit.minElevation = Math.min(limit.minElevation, elevation);
            }

            if (isUncertaintyFinite) {
                limit.maxUncertainty = Math.max(limit.maxUncertainty, uncertainty);
                limit.minUncertainty = Math.min(limit.minUncertainty, uncertainty);
            }
        }
// console.log("positions", i, positions);
        attribute.value = positions;
    }

    calculateColors(attribute) {
        const {} = this.props;
        const {nodeCount, attributeManager, limit} = this.state;
        const positions = attributeManager.attributes.positions.value;
        const colors = new Uint8ClampedArray(nodeCount * attribute.size);
        const eRange = limit.maxElevation - limit.minElevation;
        const pRange = limit.maxUncertainty - limit.minUncertainty;

        for (let i = 0; i < nodeCount; i++) {
            let pIndex = i * attributeManager.attributes.positions.state.size;
            let cIndex = i * attribute.size;
            let eRatio = eRange === 0 ? 1 : (positions[pIndex + 2] - limit.minElevation) / eRange;
            let pRatio = pRange === 0 ? 1 : (positions[pIndex + 3] - limit.minUncertainty) / pRange;

            colors[cIndex] = ((eRatio - 0.5) > 0) ? (eRatio - 0.5) * 2 * 255 : 0;
            colors[cIndex + 1] = (1 - 4 * (eRatio - 0.5) * (eRatio - 0.5)) * 255;
            colors[cIndex + 2] = ((eRatio - 0.5) > 0) ? 0 : (0.5 - eRatio) * 2 * 255;
            colors[cIndex + 3] = (pRatio * 0.7 + 0.1) * 255;


//             colors[cIndex] = (eRatio - 0.5) * 2 * 255;
//             colors[cIndex + 1] = (1 - 4 * (eRatio - 0.5) * (eRatio - 0.5)) * 255;
//             colors[cIndex + 2] = (0.5 - eRatio) * 2 * 255;
//             colors[cIndex + 3] = (eRatio * 0.7 + 0.3) * 255;
// console.log(colors[cIndex], colors[cIndex+1],colors[cIndex+2],colors[cIndex+3],eRatio);
            // vColor = vec4(2. * (ratio - 0.5), 1. - 4. * (ratio - 0.5) * (ratio - 0.5), 2. * (0.5 - ratio), ratio * 0.7 + 0.3);
        }
// console.log("colors", colors);
        attribute.value = colors;
    }

    encodePickingColor(index) {
        const {} = this.props;
        const {attributeManager, limit, latCount, lonCount} = this.state;
        const positions = attributeManager.attributes.positions.value;
        const latIndex = Math.floor(index / lonCount);
        const lonIndex = index % lonCount;

        const eRange = limit.maxElevation - limit.minElevation;
        const pIndex = index * attributeManager.attributes.positions.state.size;
        const eRatio = eRange === 0 ? 1 : (positions[pIndex + 2] - limit.minElevation) / eRange;

        return [latIndex / (latCount - 1) * 255, lonIndex / (lonCount - 1) * 255, eRatio * 255];
    }

    decodePickingColor(color) {
        const {} = this.props;
        const {limit, bound} = this.state;
        const latRange = bound.maxLat - bound.minLat;
        const lonRange = bound.maxLon - bound.minLon;
        const eRange = limit.maxElevation - limit.minElevation;
// console.log("decode", color);
        return [
            latRange === 0 ? 1 : (bound.minLat + color[0] / 255 * latRange),
            lonRange === 0 ? 1 : (bound.minLon + color[1] / 255 * lonRange),
            eRange === 0 ? 1 : (limit.minElevation + color[2] / 255 * eRange)
        ];
    }

    getPickingInfo(opts) {
        const {info} = opts;

        if (info && info.index !== -1) {
            const [latitude, longitude, elevation] = info.index;
            info.latLonEle = [latitude, longitude, elevation];
        }
// console.log(info);
        return info;
    }

    calculatePickingColors(attribute) {
        const {} = this.props;
        const {nodeCount} = this.state;
        const pickingColors = new Uint8ClampedArray(nodeCount * attribute.size);

        for (let i = 0; i < nodeCount; i++) {
            const pickingColor = this.encodePickingColor(i);
            const index = i * 3;

            pickingColors[index] = pickingColor[0];
            pickingColors[index + 1] = pickingColor[1];
            pickingColors[index + 2] = pickingColor[2];
        }
// console.log("calPickCol", pickingColors);
        attribute.value = pickingColors;
    }

    draw({uniforms}) {
        this.state.model.render(
            Object.assign({}, uniforms, {
                lightsPosition: [-100, 25, 15000],
                ambientRatio: 0.2,
                diffuseRatio: 0.9,
                specularRatio: 0.2,
                lightsStrength: [10.0, 0.0],
                numberOfLights: 1
            })
        );
// console.log("check");
    }

    /**
     * Private methods
     */

    _getShaders() {
        return {
            vs: meshGridSurfaceLayerVertex,
            fs: meshGridSurfaceLayerFragment
        };
    }

    _getModel(gl) {
        const {} = this.state;
        const shaders = this._getShaders();
        const model = new Model(gl, {
            id: this.props.id,
            vs: shaders.vs,
            fs: shaders.fs,
            modules: ['picking'],
            geometry: new Geometry({
                drawMode: GL.TRIANGLES,
                attributes: {}
            }),
            vertexCount: 0,
            isIndexed: true
        });

        return model;
    }
}

MeshGridSurfaceLayer.layerName = 'MeshGridSurfaceLayer';
MeshGridSurfaceLayer.defaultProps = defaultProps;