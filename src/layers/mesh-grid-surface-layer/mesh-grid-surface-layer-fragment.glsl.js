export default `\
#define SHADER_NAME mesh-grid-surface-layer-fragment-shader

#ifdef GL_ES
precision highp float;
#endif

varying vec4 vColor;

void main(void) {
    gl_FragColor = vColor;
    gl_FragColor = picking_filterHighlightColor(gl_FragColor);
    gl_FragColor = picking_filterPickingColor(gl_FragColor);
}
`;