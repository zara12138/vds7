export default `\
#define SHADER_NAME density-surface-layer-vertex-shader
#define HEIGHT_FACTOR 25.

attribute vec4 positions;
attribute vec4 colors;
attribute vec3 pickingColors;

varying vec4 vColor;

vec4 getWorldSpacePos(vec4 positions) {
  vec2 pos = project_position(positions.xy);
  float elevation = project_scale(positions.z * 100.);
  vec3 extrudedPosition = vec3(pos.xy, elevation + 1.0);
  vec4 position_worldspace = vec4(extrudedPosition, 1.0);
  return position_worldspace;
}

void main(void) {
    vec4 position_worldspace = getWorldSpacePos(positions);
    gl_Position = project_to_clipspace(position_worldspace);
    // float lightWeight = getLightWeight(position_worldspace.xyz, 1.0);
    vColor = vec4(colors.rgb, colors.a) / 255.0;
    picking_setPickingColor(pickingColors);
}
`;